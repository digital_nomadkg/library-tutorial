package com.sda.tutorial.springannotation.librarytutorial;

import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Library {
    private List<Book> bookList;
    private String name;
    private String address;

    public List<Book> getBookList() {
        return bookList;
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Library{" +
                "bookList=" + bookList +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}

