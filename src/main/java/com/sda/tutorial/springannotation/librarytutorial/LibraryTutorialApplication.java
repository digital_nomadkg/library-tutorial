package com.sda.tutorial.springannotation.librarytutorial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class LibraryTutorialApplication {

    public static void main(String[] args) {
        SpringApplication.run(LibraryTutorialApplication.class, args);

        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        // retrieve bean from spring container
        Book book = context.getBean("book", Book.class);

        Library library = context.getBean("library", Library.class);
        library.setAddress("Tallinn");
        library.setName("test");
        book.setAuthor("Kirves");
        book.setPublisher("Springfield");
        book.setId(1);
        List<Book> books=new ArrayList<>();
        books.add(book);
        library.setBookList(books);

        System.out.println(library.toString());
        //System.out.println(book.toString());
        context.close();
    }
}
